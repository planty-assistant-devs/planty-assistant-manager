import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { PlantyAssistantManagerAgentModule } from './agent/agent.module';
import { PlantyAssistantManagerPairingRequestModule } from './pairing-request/pairing-request.module';
import { PlantyAssistantManagerSkillModule } from './skill/skill.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    // prettier-ignore
    imports: [
        PlantyAssistantManagerAgentModule,
        PlantyAssistantManagerPairingRequestModule,
        PlantyAssistantManagerSkillModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PlantyAssistantManagerEntityModule {}
